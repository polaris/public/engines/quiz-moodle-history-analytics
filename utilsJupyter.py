import json
import os
import re
import requests

PAGE_SIZE = 100

#this module loads statements from a json file and saves results to a json file instead of accessing the stupla x api
# file_to_read = 'data/stupla_x_api_statements.json'
file_to_read = 'data/buffer_multiple_quiz_10_users.json'
file_name = file_to_read.split('/')[-1]

def get_statements_page(api_url, analytics_token, last_object_id=None):
    print(api_url)
    payload = (
        {"page_size": PAGE_SIZE, "last_object_id": last_object_id}
        if last_object_id
        else {"page_size": PAGE_SIZE}
    )
    response = requests.post(
        f"{api_url}/api/v1/provider/data",
        headers={"Authorization": f"Basic {analytics_token}"},
        json=payload,
    )
    response.raise_for_status()

    payload = response.json()
    return payload.get("statements", [])


def get_statements(api_url, analytics_token):
    # reads data from local file instead of api
    statements = []
    try:
        with open(file_to_read) as json_file:
            data = json.load(json_file)
    except:
        try:
            data = read_broken_json(file_to_read)
        except:
            raise Exception("Could not read file. Please check if the file exists and is valid json.")
    statements.extend(data)
    return statements

def read_broken_json(datafile):
    with open(datafile, 'r') as file:
        content = file.read()

    content = '[' + content + ']'

    content = re.sub('}\n{', '},\n{', content)

    data = json.loads(content)
    return data

def get_existing_results(api_url, analytics_token):
    response = requests.get(
        f"{api_url}/api/v1/provider/results",
        headers={"Authorization": f"Basic {analytics_token}"},
    )
    response.raise_for_status()
    data = response.json()
    return data

def save_results(api_url, analytics_token, results):
    #save results to file in data/results.json
    if not os.path.exists('results'):
        os.makedirs('results')

    with open('results/' + results['context_id'] + '.json', 'w') as outfile:
        json.dump(results, outfile,indent=4)

#%%
