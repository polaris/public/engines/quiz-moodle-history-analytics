import sys
import time

from collections import defaultdict
from requests import HTTPError
import statistics

#for lacal testing. If you want to use local data, add "load_local_data" as third argument
if len(sys.argv) > 3 and sys.argv[3] == "load_local_data":
    from utilsJupyter import get_statements, save_results
else:
    from utils import get_statements, save_results



def calculate_quiz_statistics(scores):
    if not scores:
        return None
    mean = statistics.mean(scores)
    median = statistics.median(scores)
    minimum = min(scores)
    maximum = max(scores)
    return {"mean": mean, "median": median, "min": minimum, "max": maximum}

def extract_context(grouping_list):
    type_mapping = {}
    for context_entry in grouping_list:
        type = context_entry['definition']['type'].split('/')[-1]
        type_mapping[type] = context_entry
    return type_mapping

def group_statements_with_scores(statements):
    #creating data structure for the output file
    output = {
        "actors": defaultdict(lambda: {
            "taken_quizzes": defaultdict(lambda: {
                "submitted_scores": [],
                "submitted_answers": []
            }),
            "verb_counts": defaultdict(int)
        }),
        "courses": defaultdict(lambda: {
            "quizzes": defaultdict(lambda: {
                "name": "",
                "scores": defaultdict(list),
                "answers": defaultdict(list),
                "stats": None
            })
        })
    }

    for statement in statements:
        verb = statement["verb"]['id'].split('/')[-1]

        actor = statement["actor"]
        actor_name = actor["account"]["name"]
        actor_key = actor_name

        output["actors"][actor_key]["verb_counts"][verb] += 1

        timestamp = statement["timestamp"]

        match verb:
            case "answered" | "leftUnanswered":
                score = statement["result"]['score']
                scored_normalized = (score['raw'] - score['min']) / score['max']
                context = extract_context(statement['context']['contextActivities']['grouping'])
                quiz_id = context['quiz']['id']
                course_id = context['course']['id']
                question_id = statement['object']['id'].split("=")[-1] # Extract question id
                answer = {
                    "score": scored_normalized,
                    "quiz_id": quiz_id,
                    "answered": verb == "answered",
                    "timestamp": timestamp,
                    "answer": statement["result"]["response"],
                    "question_id": question_id,
                    "course_id": course_id
                }
                output["actors"][actor_key]["taken_quizzes"][quiz_id]["submitted_answers"].append(answer)
                output["courses"][course_id]["quizzes"][quiz_id]["answers"][actor_key].append(answer)

            case "submitted":
                score = statement["result"]['score']['raw']
                context = extract_context(statement['context']['contextActivities']['grouping'])
                quiz_id = context['quiz']['id']
                course_id = context['course']['id']
                output["actors"][actor_key]["taken_quizzes"][quiz_id]["submitted_scores"].append({
                    "score": score,
                    "quiz_id": quiz_id,
                    "timestamp": timestamp
                })
                output["courses"][course_id]["quizzes"][quiz_id]["name"] = context['quiz']['definition']['name']
                output["courses"][course_id]["quizzes"][quiz_id]["scores"][actor_key].append(score)

            case "accessed":
                pass

            case "started":
                pass

            case "graded":
                pass

            # case "leftUnanswered":
            #     pass

            case "updated":
                pass

            case _:
                pass

    return output

def main(analytics_token, api_url):
    try:
        # Retrieve available statements
        statements = get_statements(api_url, analytics_token)
        grouped_statements = group_statements_with_scores(statements)

        # Calculate statistics
        for course_id in grouped_statements["courses"]:
            for quiz_id in grouped_statements["courses"][course_id]["quizzes"]:
                quiz_scores = []
                quiz_data = grouped_statements["courses"][course_id]["quizzes"][quiz_id]["scores"]
                for scores_group in quiz_data.values():
                    for score in scores_group:
                        quiz_scores.append(score)
                grouped_statements["courses"][course_id]["quizzes"][quiz_id]["stats"] = calculate_quiz_statistics(quiz_scores)


        description = {
            "de": "Placeholder",
            "en": "placeholder",
        }

        # Send result to rights engine
        result_time = int(time.time())
        save_results(
            api_url,
            analytics_token,
            {'context_id': 'All', 'time': result_time, "result": grouped_statements, "description": description},
        )
        for actor_id, actor in grouped_statements["actors"].items():
            save_results(
                api_url,
                analytics_token,
                {'context_id': 'user_' + actor_id, 'time': result_time, "result": actor, "description": description},
            )

        for quiz_id, quiz in grouped_statements["courses"].items():
            quiz_id = quiz_id.split("=")[-1]
            save_results(
                api_url,
                analytics_token,
                {'context_id': 'quiz_' + quiz_id, 'time': result_time, "result": quiz, "description": description},
            )

    except HTTPError as error:
        print(error.response.status_code, error.response.text)


if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])